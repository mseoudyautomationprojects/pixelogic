package Pages;

import com.relevantcodes.extentreports.LogStatus;
import executionEngine.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.ScreenShotRobot;
import utils.ExtentReports.ExtentTestManager;

public class RegistrationPage extends BasePage {


    public RegistrationPage(WebDriver driver) { super(driver); }


    @FindBy(name = ("firstname"))
    WebElement firstName;
    @FindBy(name = ("lastname"))
    WebElement lastName;
    @FindBy(name = ("phone"))
    WebElement mobileNumber;
    @FindBy(name = ("email"))
    WebElement email;
    @FindBy(name = ("password"))
    WebElement pass;
    @FindBy(name = ("confirmpassword"))
    WebElement confirmPass;
    @FindBy(xpath = ("//button[contains(text(),'Sign Up')]"))
    WebElement signUp_btn;

    @FindBy(id =("dropdownCurrency"))
    WebElement succssfulLogin;

    @FindBy(xpath = ("//*[@id=\"//header-waypoint-sticky\"]/div[1]/div/div/div[2]/div/ul/li[3]/div/div/div/a[2]"))
    WebElement logOut;

    @Step("user should Register with valid credential")
    public RegistrationPage SignUp(String firstname,String lastname,String phone,String Email,String password, String confirmPassword) throws Exception
    {

        firstName.sendKeys(firstname);
        ScreenShotRobot.takeScreenshot(driver,firstname);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his firstname");
        lastName.sendKeys(lastname);
        ScreenShotRobot.takeScreenshot(driver,lastname);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his lastname");
        mobileNumber.sendKeys(phone);
        ScreenShotRobot.takeScreenshot(driver,phone);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his phone");
        email.sendKeys(Email);
        ScreenShotRobot.takeScreenshot(driver,Email);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his Email");
        pass.sendKeys(password);
        ScreenShotRobot.takeScreenshot(driver,password);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his password");
        confirmPass.sendKeys(confirmPassword);
        ScreenShotRobot.takeScreenshot(driver,confirmPassword);
        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his confirmPassword");

        signUp_btn.click();
        signUp_btn.click();
        signUp_btn.click();

        ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his credential for Registration and clicked sign up");
        ScreenShotRobot.takeScreenshot(driver,"user entered his credential for Registration and clicked sign up");

        wait(3000);

        succssfulLogin.click();
        ExtentTestManager.getTest().log(LogStatus.PASS, "user successfully registered");
        ScreenShotRobot.takeScreenshot(driver,"user successfully registered");

        logOut.click();
        return this;
    }
}
