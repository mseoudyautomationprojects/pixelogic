package Pages;

import com.relevantcodes.extentreports.LogStatus;
import executionEngine.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.ScreenShotRobot;
import utils.ExtentReports.ExtentTestManager;

public class LoginPage extends BasePage {

	public LoginPage(WebDriver driver) { super(driver); }

	@FindBy(name = ("username"))
	WebElement user_name;
	@FindBy(name = ("password"))
	WebElement pass;
	@FindBy(xpath = ("//*[@id=\"loginfrm\"]/button"))
	WebElement login_Btn;

	@Step("user should login with valid credential")
	public LoginPage login(String email,String password) throws Exception
	{
		
		user_name.sendKeys(email);
		ScreenShotRobot.takeScreenshot(driver,email);
		pass.sendKeys(password);
		ScreenShotRobot.takeScreenshot(driver,password);
		ExtentTestManager.getTest().log(LogStatus.PASS, "user entered his credential");
		ScreenShotRobot.takeScreenshot(driver,"user entered his credential");
		login_Btn.click();
		return this;
	}
}
