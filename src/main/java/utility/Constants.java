package utility;



public class Constants {
	
	 //System Variables
	public static final String workingDir = System.getProperty("user.dir");
	public static final String Path_TestData = workingDir+"\\TestData\\";
	public static final String File_TestData = "TestData.xlsx";
	public static final String ExtentReportResults= workingDir+"\\ExtentReports\\ExtentReportResults.html";


	//Test Data Sheet Columns

	public static final int Col_TestCaseName = 1;
	public static final int Col_TestID = 0;
	public static final int Col_UserName =2 ;
	public static final int Col_Password = 3;
	public static final int Col_Message = 4;

	public static final int Col_ObjectName=2 ;
	public static final int Col_ObjectNamevalue=3;

	public static final int Col_FirstName=4 ;
	public static final int Col_LastName=5;
}