package utility;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import utils.ExtentReports.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Utils {
	ExtentReports report;
	static ExtentTest logger;
	 public static void takeScreenshot(WebDriver driver, String sTestCaseName) throws Exception{
			try{
				       		
        	        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
        	                getScreenshotAs(OutputType.BASE64);

        	        ExtentTestManager.getTest().log(LogStatus.INFO,sTestCaseName,
        	                ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        		
			} catch (Exception e){
				ExtentTestManager.getTest().log(LogStatus.ERROR,"Class Utils | Method takeScreenshot | Exception occured while capturing ScreenShot : "+e.getMessage());
				throw new Exception();
			}
		}
	 
	 
	 public static String getTestCaseName(String sTestCase)throws Exception{
			String value = sTestCase;
			try{
				int posi = value.indexOf("@");
				value = value.substring(0, posi);
				posi = value.lastIndexOf(".");	
				value = value.substring(posi + 1);
				return value;
					}catch (Exception e){
						ExtentTestManager.getTest().log(LogStatus.ERROR,"getTestCaseName"+e.getMessage());
				throw (e);
						}
	
	 }
}

	
				

	 
