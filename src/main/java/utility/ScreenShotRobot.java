package utility;

import javax.imageio.ImageIO;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import utils.ExtentReports.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

import io.qameta.allure.Attachment;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Credential: Shantonu Sarker on 9/11/16. (Thanks to him.)
 */

public class ScreenShotRobot {

    //Text attachments for Allure
    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] saveScreenshotPNG (WebDriver driver) {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }
    
    //Text attachments for Allure
    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog (String message) {
        return message;
    }
    
    public void saveAsImage(byte[] imageAsByteArray, String name) {
        InputStream in = new ByteArrayInputStream(imageAsByteArray);
        BufferedImage bImageFromConvert = null;
        
        File file;
        try {
            file = new File("./Images/"+name);
            bImageFromConvert = ImageIO.read(new ByteArrayInputStream(imageAsByteArray));
            ImageIO.write(bImageFromConvert, "jpg", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void capture(String filePath) {
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            File file = new File(filePath);
            ImageIO.write(screencapture, "jpg", file);

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void capturePNG(String filePath) {
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            File file = new File(filePath);
            ImageIO.write(screencapture, "png", file);

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] capturePNG() {
        byte[] out = null;
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ImageIO.write(screencapture, "png", bo);
            out = bo.toByteArray();
            bo.close();

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }

    public static byte[] capture() {
        byte[] out = null;
        try {
            BufferedImage screencapture = new Robot().createScreenCapture(
                    new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ImageIO.write(screencapture, "jpg", bo);
            out = bo.toByteArray();
            bo.close();
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }
    
    public static void  takeScreenshot(WebDriver driver,String iTestResult){
    	//Get driver from BaseTest and assign to local webdriver variable.
    

        //Allure ScreenShotRobot and SaveTestLog
        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test case:" + iTestResult);
            saveScreenshotPNG(driver);
        }

        //Save a log on allure.
        saveTextLog(iTestResult + " screenshot taken!");

        //Take base64Screenshot screenshot for extent reports
        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
                getScreenshotAs(OutputType.BASE64);

        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.getTest().log(LogStatus.PASS,"TakesScreenshot"+iTestResult,
                ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
    }
}