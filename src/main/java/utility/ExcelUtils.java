package utility;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;

public class ExcelUtils {

    private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static org.apache.poi.ss.usermodel.Cell Cell;
    private static XSSFRow Row;
    //private static XSSFRow Row;

public static void setExcelFile(String Path) throws Exception {
    try {
        FileInputStream ExcelFile = new FileInputStream(Path);
        ExcelWBook = new XSSFWorkbook(ExcelFile);
    } catch (Exception e){
        Log.error("Class Utils | Method setExcelFile | Exception desc : "+e.getMessage());
        }
    }

public static String getCellData(int RowNum, int ColNum, String SheetName ) throws Exception{
    try{
        ExcelWSheet = ExcelWBook.getSheet(SheetName);
           Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
           DataFormatter formatter = new DataFormatter();
        String CellData = formatter.formatCellValue(Cell);
        return CellData;
     }catch (Exception e){
         Log.error("Class Utils | Method getCellData | Exception desc : "+e.getMessage());
         return"";
         }
     }


public static int getRowCount(String SheetName){
    int iNumber=0;
    try {
        ExcelWSheet = ExcelWBook.getSheet(SheetName);
        iNumber=ExcelWSheet.getLastRowNum()+1;
    } catch (Exception e){
        Log.error("Class Utils | Method getRowCount | Exception desc : "+e.getMessage());
        }
    return iNumber;
    }

public static int getRowContains(String sTestCaseName, int colNum,String SheetName) throws Exception{
    int iRowNum=0;
    try {
        //ExcelWSheet = ExcelWBook.getSheet(SheetName);
        int rowCount = ExcelUtils.getRowCount(SheetName);
        for (; iRowNum<rowCount; iRowNum++){
            if  (ExcelUtils.getCellData(iRowNum,colNum,SheetName).equalsIgnoreCase(sTestCaseName)){
                break;
            }
        }
    } catch (Exception e){
        Log.error("Class Utils | Method getRowContains | Exception desc : "+e.getMessage());
        }
    return iRowNum;
    }
        public static int getRowUsed() throws Exception {
            try{
                int RowCount = ExcelWSheet.getLastRowNum();
                Log.info("Total number of Row used return as < " + RowCount + " >.");
                return RowCount;
            }catch (Exception e){
                Log.error("Class ExcelUtil | Method getRowUsed | Exception desc : "+e.getMessage());
                System.out.println(e.getMessage());
                throw (e);
            }

        }
}