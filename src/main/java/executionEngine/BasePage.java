package executionEngine;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
	public static WebDriver driver;


	// Constructor
	public BasePage(WebDriver driver) {
		BasePage.driver = driver;

	// initialize main driver

		// page factory to use POM
		PageFactory.initElements(driver, this);
	}
}
