package executionEngine;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utility.Log;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class BaseTest {

	public static WebDriver driver;

	DesiredCapabilities dc = new DesiredCapabilities();
	private String sTestCaseName;

	@BeforeClass

	public void setUp() throws Exception {

		try {
			// ****** log create *******
			sTestCaseName = this.toString();
			sTestCaseName = utility.Utils.getTestCaseName(this.toString());

			Log.startTestCase(sTestCaseName);

			// ****** Driver Connect *******
			{
				String value;
				value = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";
				System.setProperty("webdriver.chrome.driver", value);


				driver=new ChromeDriver();
				driver.manage().window().maximize();

				driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
				driver.navigate().to("https://phptravels.net/register");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				System.out.println(driver.getTitle());

			}


		} catch (Exception e) {
			Log.error("setUp" + " Exception desc : " + e.getMessage());

			throw (e);
		}

	}


	@AfterClass
	public void teardown() {
		// driver.quit();
		driver.close();
	}

}
