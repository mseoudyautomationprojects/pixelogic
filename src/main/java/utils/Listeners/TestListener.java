package utils.Listeners;

import executionEngine.BaseTest;
import utility.Log;
import utils.ExtentReports.ExtentManager;
import utils.ExtentReports.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;

import java.awt.*;
import java.io.File;
import java.io.IOException;


public class TestListener extends BaseTest implements ITestListener {

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    //Text attachments for Allure
    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshotPNG (WebDriver driver) {
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    //Text attachments for Allure
    @Attachment(value = "{0}", type = "text/plain")
    public static String saveTextLog (String message) {
        return message;
    }

    //HTML attachments for Allure
    @Attachment(value = "{0}", type = "text/html")
    public static String attachHtml(String html) {
        return html;
    }

    @SuppressWarnings("UnusedReturnValue")
    @Attachment(value = "Test Log", type = "text/plain")
    public String stopCatch() {
        String result = MultithreadedConsoleOutputCatcher.getContent();
       // ExtentTestManager.getTest().log(LogStatus.INFO, "Test Log", result);
        MultithreadedConsoleOutputCatcher.stopCatch();
        return result;
    }
    
    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println("I am on Start method " + iTestContext.getName());
        Log.startTestCase("I am on Start method " + iTestContext.getName());
        iTestContext.setAttribute("WebDriver", driver);
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        System.out.println("I am on Finish method " + iTestContext.getName());
        Log.endTestCase("I am on Start method " + iTestContext.getName());

        //Do tier down operations for extentReports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        File htmlfile= new File(System.getProperty("user.dir")+"\\ExtentReports\\ExtentReportResults.html");
       
        try{
        	Desktop.getDesktop().browse(htmlfile.toURI());
        }catch(IOException e ) {
        	e.printStackTrace();
        }
        }
  

    @Override
    public void onTestStart(ITestResult iTestResult) {
    	MultithreadedConsoleOutputCatcher.startCatch();
        System.out.println("I am on TestStart method " +  getTestMethodName(iTestResult) + " start");
        Log.info(getTestMethodName(iTestResult) + " test is starting.");
        //Start operation for extentreports.
        //ExtentTestManager.startTest(iTestResult.getMethod().getMethodName(),"");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("I am on TestSuccess method " +  getTestMethodName(iTestResult) + " succeed");
        Log.info("I am on TestSuccess method " +  getTestMethodName(iTestResult) + " succeed");
        //Extentreports log operation for passed tests.
        
      //Get driver from BaseTest and assign to local webdriver variable.
        Object testClass = iTestResult.getInstance();


        //Allure ScreenShotRobot and SaveTestLog
        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test case:" + getTestMethodName(iTestResult));
            saveScreenshotPNG(driver);
        }

        //Save a log on allure.
        saveTextLog(getTestMethodName(iTestResult) + " failed and screenshot taken!");

        //Take base64Screenshot screenshot for extent reports
        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
                getScreenshotAs(OutputType.BASE64);

        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.getTest().log(LogStatus.PASS,"tackeScreenShots",
                ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        
        ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
        
        stopCatch ();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("I am in onTestFailure method " +  getTestMethodName(iTestResult) + " failed");
        Log.info("I am in onTestFailure method " +  getTestMethodName(iTestResult) + " failed");
        //Get driver from BaseTest and assign to local webdriver variable.
        Object testClass = iTestResult.getInstance();


        //Allure ScreenShotRobot and SaveTestLog
        if (driver instanceof WebDriver) {
            System.out.println("Screenshot captured for test case:" + getTestMethodName(iTestResult));
            saveScreenshotPNG(driver);
        }

        //Save a log on allure.
        saveTextLog(getTestMethodName(iTestResult) + " failed and screenshot taken!");

        //Take base64Screenshot screenshot for extent reports
        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
                getScreenshotAs(OutputType.BASE64);

        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.getTest().log(LogStatus.FAIL,"Test Failed",
                ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
        stopCatch ();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println("I am in onTestSkipped method "+  getTestMethodName(iTestResult) + " skipped");
        Log.info("I am in onTestSkipped method "+  getTestMethodName(iTestResult) + " skipped");

        //Extentreports log operation for skipped tests.
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
        stopCatch ();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
        Log.info("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
        stopCatch ();
    }

}
