package tests;

import executionEngine.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.LoginPage;
import utility.Constants;
import utility.ExcelUtils;
import utils.ExtentReports.ExtentTestManager;
import utils.Listeners.TestListener;

@Listeners({ TestListener.class })
@Epic("Login Test")
@Feature("Login Test")

public class TestLoginPage extends BaseTest {
	
	LoginPage loginPageObject;
	@Severity(SeverityLevel.BLOCKER)
	@Description("Test Description:  login")
	@Story("login.")
	@Test(priority = 0, description = "login")
	public void testValidLogin() throws Exception {
		loginPageObject = new LoginPage(driver);
		ExtentTestManager.startTest("testValidLogin", "testValidLogin");
		ExcelUtils.setExcelFile(Constants.Path_TestData + Constants.File_TestData);

		String username = ExcelUtils.getCellData(1, 1, "Sheet1");
		String password = ExcelUtils.getCellData(1, 2, "Sheet1");
		loginPageObject.login(username, password);
	}

}
