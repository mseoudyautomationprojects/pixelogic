package tests;

import Pages.LoginPage;
import Pages.RegistrationPage;
import executionEngine.BaseTest;
import io.qameta.allure.*;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utility.Constants;
import utility.ExcelUtils;
import utils.ExtentReports.ExtentTestManager;
import utils.Listeners.TestListener;

@Listeners({ TestListener.class })
@Epic("Full Secenario Test")
@Feature("Full Secenario Test")

public class TestFullSecenario extends BaseTest {

    RegistrationPage registrationPageObject;
    @Severity(SeverityLevel.BLOCKER)
    @Description("Test Description:  Registration Test")
    @Story("Registration Test")
    @Test(priority = 0, description = "Registration Test")
    public void testValidRegisteration() throws Exception {
        registrationPageObject = new RegistrationPage(driver);

        ExtentTestManager.startTest("testValidRegisteration", "testValidRegisteration");
        ExcelUtils.setExcelFile(Constants.Path_TestData + Constants.File_TestData);

        String firstname = ExcelUtils.getCellData(1, 1, "Sheet1");
        String lastname = ExcelUtils.getCellData(1, 2, "Sheet1");
        String phone = ExcelUtils.getCellData(1, 3, "Sheet1");
        String Email = ExcelUtils.getCellData(1, 4, "Sheet1");
        String password = ExcelUtils.getCellData(1, 5, "Sheet1");
        String confirmPassword = ExcelUtils.getCellData(1, 6, "Sheet1");

        registrationPageObject.SignUp(firstname,lastname,phone,Email,password,confirmPassword);


    }

    LoginPage loginPageObject;
    @Severity(SeverityLevel.BLOCKER)
    @Description("Test Description:  login")
    @Story("login.")
    @Test(priority = 1, description = "login", dependsOnMethods = "testValidRegisteration")
    public void testValidLogin() throws Exception {
        loginPageObject = new LoginPage(driver);
        ExtentTestManager.startTest("testValidLogin", "testValidLogin");
        ExcelUtils.setExcelFile(Constants.Path_TestData + Constants.File_TestData);

        String Email = ExcelUtils.getCellData(1, 4, "Sheet1");
        String password = ExcelUtils.getCellData(1, 5, "Sheet1");
        loginPageObject.login(Email, password);
    }
}
